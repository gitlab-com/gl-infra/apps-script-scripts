# On-call updater

This is just a very simple script intended to be executed daily that checks the next 30 days for on-call dates and automatically creates an all day event showing that you are on-call that day which can aid interview schedulers to not schedule interviews for those dates.

This does NOT interact with the PagerDuty API in anyway and it automatically has permissions to your calendar so there are NO keys to set. It operates by reading your exported calendar and modifying your personal calendar accordingly.

## Pre-requisites

You must be [subscribed to your pagerduty calendar](https://support.pagerduty.com/docs/schedules-in-apps#calendar-apps).

## Usage

This script relies on [script properties](https://developers.google.com/apps-script/guides/properties?hl=en#manage_script_properties_manually) to work. When you create the project in Apps Script you'll want to set the following parameters (at least SRE is required):

| Key | Value | Default |
|----------|----------|----------|
| ALL_DAY_EVENT | If you want a custom name for the all day even created | On Call - No Interviews |
| SRE | Your name as in the title of your exported PagerDuty calendar | Nobody |
