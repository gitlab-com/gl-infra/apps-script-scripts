const scheduleName = fetchProperty("SCHEDULE","On Call - GitLab Dedicated Platform Escalation")
const allDayEventName = fetchProperty("ALL_DAY_EVENT", "On Call - No Interviews")
const sreName = fetchProperty("SRE",'Nobody')

function myFunction() {
  let now = new Date();
  let thirtyDaysFromNow = new Date(now.getTime() + (30 * 24 * 60 * 60 * 1000))
  let pdCalendar = discoverPagerDutyCalendar(sreName);
  let mainCalendar = CalendarApp.getDefaultCalendar();
  
  clearMainCal(mainCalendar, now, thirtyDaysFromNow)


  let events = pdCalendar.getEvents(now, thirtyDaysFromNow)

  
  for (event of events) {
    if (event.getTitle() != scheduleName) {
      continue;
    }

    let onCallDay = event.getStartTime()

    Logger.log(onCallDay)
    
    let dayEvents = mainCalendar.getEventsForDay(onCallDay)
    let createEvent = dayEvents.every(function(e, _, _) { return e.getTitle() != allDayEventName})
    
    if (createEvent) {
      mainCalendar.createAllDayEvent(allDayEventName, onCallDay)
    }
  
  }

}

function discoverPagerDutyCalendar(name) {
  cal = CalendarApp.getCalendarsByName('On Call Schedule for ' + name)[0]
  Logger.log(cal.getName())

  return cal
}

function clearMainCal(cal, start, end) {
  events = cal.getEvents(start, end)
  
  events.forEach(function(event, _, _){
    if (event.getTitle() == allDayEventName) {
      event.deleteEvent()
    }
  })
}

function fetchProperty(key, defaultVal) {
  if (PropertiesService.getScriptProperties().getProperty(key) == null) {
    return defaultVal
  } else {
    return PropertiesService.getScriptProperties().getProperty(key)
  }
}